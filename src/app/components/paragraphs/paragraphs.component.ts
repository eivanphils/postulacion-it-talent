import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-paragraphs',
  templateUrl: './paragraphs.component.html',
  styleUrls: ['./paragraphs.component.css'],
  providers: [ApiService, HelperService]
})
export class ParagraphsComponent implements OnInit {
  public loading = false;
  // tslint:disable-next-line: max-line-length
  public paragraphsList: any[] = [];
  public hasError = false;

  constructor(
    private apiService: ApiService,
    public helperService: HelperService
  ) { }

  ngOnInit() {}

  loadData() {
    this.loading = true;
    this.paragraphsList = [];

    // se llama endpoit para obtener listado de parrafos
    this.apiService.getArrayParagraphs().subscribe(
    async (response) => {
      if(!response.success && response.error) {
        this.hasError = true;
        return;
      }

      for (let i = 0; i < response.data.length; i++) {
        const data: any = {};
        data.index = i + 1;

        // se llaman metodos dentro del servicio para procesar el parrafo
        data.alphabet = await this.helperService.getLetterOccurrences(response.data[i].paragraph);
        data.sumNumbers = await this.helperService.getSumNumbers(response.data[i].paragraph);

        this.paragraphsList.push(data);
        this.loading = false;
      }
    },
    (error) => {
      this.loading = false;
      this.hasError = true;
    });
  }
}
