import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css'],
  providers: [ApiService, HelperService]
})
export class BoardsComponent implements OnInit {
  public numbersData: any[] = [];
  public orderedNumbers: any;

  public loading = false;

  constructor(
    private apiService: ApiService,
    private helperService: HelperService
  ) { }

  ngOnInit() {}

  loadData() {

    this.loading = true;
    this.apiService.getArrayNumbers().subscribe(
      (response) => {
        if (!response.success) {
          this.loading = false;
          return;
        }

        let i = 0;
        response.data.forEach((number) => {

          // se valida si existe el numero para no repetir el mismo en el arreglo
          if(!this.numbersData.find(x => x.number === number)) {
            const dataBody = {
              index: i++,
              number: number,
              quantity: this.helperService.getNumberOccurrences(number, response.data),
              firstPosition: 0,
              lastPosition: response.data.length - 1
            };

            this.numbersData.push(dataBody);
          }

        });

        this.orderedNumbers = this.helperService.sorterNumbers(this.numbersData.slice());

        this.loading = false;
      },
      (error) => {
        this.loading = false;
      }
    );

  }
}
