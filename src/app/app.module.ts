import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardsComponent } from './components/boards/boards.component';
import { ApiService } from './services/api.service';
import { FormsModule } from '@angular/forms';
import { ParagraphsComponent } from './components/paragraphs/paragraphs.component';
import { HelperService } from './services/helper.service';

@NgModule({
  declarations: [
    AppComponent,
    BoardsComponent,
    ParagraphsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    ApiService,
    HelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
