import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardsComponent } from './components/boards/boards.component';
import { ParagraphsComponent } from './components/paragraphs/paragraphs.component';

const routes: Routes = [
  { path: '', redirectTo: 'boards', pathMatch: 'full'},
  { path: 'boards', component: BoardsComponent},
  { path: 'paragraph', component: ParagraphsComponent},
  { path: '**', redirectTo: 'boards'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }