export interface Response {
    success: boolean;
    data: any[];
    error?: boolean;
}