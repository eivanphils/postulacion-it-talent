import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Response } from '../interfaces/interfaces';
const url = 'http://168.232.165.184';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getArrayNumbers() {
    return this.http.get<Response>(`${url}/prueba/array`);
  }

  getArrayParagraphs() {
    return this.http.get<Response>(`${url}/prueba/dict`);
  }
}
