import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  // tslint:disable-next-line: max-line-length
  public letterInAlphabet: string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

  constructor() { }

  public getLetterOccurrences(paragrahp: string) {
    return new Promise(resolve => {
      const occurrences = [];

      // for para recorrer cada caracter de la cadena de texto
      for (let i = 0; i < paragrahp.length; i++) {

        let k = 0;
        // for para recorrer el arreglo que contiene las letras del abecedario
        for (let j = 0; j < this.letterInAlphabet.length; j++) {
          if (paragrahp[i].toLowerCase() === this.letterInAlphabet[j]) {
            k++;

            // retorno el indice del objeto en caso de que ya este guardado para calcular la cantidad de veceses que aparece
            const index = occurrences.findIndex(item => item.letra === this.letterInAlphabet[j]);

            // si existe se actualiza el valor de ocurrencia
            if (index !== -1) {
              occurrences[index].occurrenceNumber = occurrences[index].occurrenceNumber + 1;
            } else {
              // si no existe se crea un nuevo registro
              const data = {
                letra: this.letterInAlphabet[j],
                occurrenceNumber: k
              };
              occurrences.push(data);
            }
          } else {
            const index = occurrences.findIndex(item => item.letra === this.letterInAlphabet[j]);
            // si no existe se crea un nuevo registro
            if (index === -1) {

              const data = {
                letra: this.letterInAlphabet[j],
                occurrenceNumber: 0
              };
              console.log('data', data);

              occurrences.push(data);
            }
          }
        }

        occurrences.sort(this.compare);
      }
      resolve(occurrences);
    });
  }

  public getSumNumbers(text: string) {
    return new Promise(resolve => {
      // se aplica regex para obtener todos los numeros de la cadena
      const regex = /[+-]?\d+(?:\.\d+)?/g;
      let match = regex.exec(text);
      let sum = 0;

      if (match !== null) {
        do {
          sum = sum + Number(match[0]);
        } while (match = regex.exec(text));
      }
      resolve(sum);
    });
  }

  private compare(a, b) {
    // ignoro las mayusculas para realizar la comparacion de que valor va sobre el otro
    const valueA = a.letra.toUpperCase();
    const valueB = b.letra.toUpperCase();
    let comparison = 0;
    if (valueA > valueB) {
      comparison = 1;
    } else if (valueA < valueB) {
      comparison = -1;
    }
    return comparison;
  }

  // metodo para obtener la cantidad de ocurrencias de un numero
  public getNumberOccurrences(value, array) {
    let counter = 0;
    for (let i = 0; i < array.length; i++) {
      if(array[i] === value) {
        counter++;
      }
    }
    return counter;
  }

  // metodo para ordenar el aareglo de objecto y retornar un arreglo de numeros
  public sorterNumbers(array) {
    const count = array.length - 1;
    let swap;

    for (let j = 0; j < count; j++) {
        for (let i = 0; i < count; i++) {

            if (array[i].number > array[i + 1].number) {
                swap = array[i + 1];
                array[i + 1] = array[i];
                array[i] = swap;
            }

        }
    }
    return array.map(x =>  x.number );
  }
}
